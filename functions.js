
async function cargarMunicipios() {
    //Generar petición http
    const peticion = await fetch('https://www.datos.gov.co/resource/xdk5-pm3f.json');
    //Capturar datos en formato json
    const data = await peticion.json();
    //Ordenar arreglo
    /*
    let arregloMunicipios = [];
    data.forEach(element => {
        arregloMunicipios.push(element.municipio);
    });
    arregloMunicipios.sort();
    */
    data.sort((a, b) => {
        if (a.municipio > b.municipio) {
            return 1;
        } else {
            return -1;
        }
    });
    //console.log(data);
    let municipios = document.getElementById('municipios');
    //Ejercicio: Cargar los municipios en el select
    data.forEach(element => {
        //console.log("Municipio: ", element.municipio);
        municipios.innerHTML += "<option value='" + element.municipio + "'>" + element.municipio + "</option>";
    });
}

var personas = [];

//-----------CESAR CAMILO------------

function limpiar() {
    document.getElementById('nombre').value = '';
    document.getElementById('apellido').value = '';
    document.getElementById('email').value = '';
    document.getElementById('municipios').value = '';
}

function listar() {
    //Obtener el tbody
    let lista = document.getElementById('lista');
    lista.innerHTML = '';
    let fila = 0;
    personas.forEach(element => {
        let id = "f" + fila;
        lista.innerHTML += `<tr>
        <td id=`+ id + `c0>` + element.nombre + `</td>
        <td id=`+ id + `c1>` + element.apellido + `</td>
        <td id=`+ id + `c2>` + element.email + `</td>
        <td id=`+ id + `c3>` + element.municipio + `</td>
        <td>
            <button type="button" class="btn btn-primary" onclick="editar('`+ id + `')">Editar</button>
            <button type="button" class="btn btn-danger" onclick="btnEliminar('`+element.email+`')">Eliminar</button>
        </td>
    </tr>`
        ++fila;
    });
}

function registrar() {
    let nombre = document.getElementById('nombre').value;
    let apellido = document.getElementById('apellido').value;
    let email = document.getElementById('email').value;
    let municipio = document.getElementById('municipios').value;
    let objPersona = {
        nombre,
        apellido,
        email,
        municipio
    }
    personas.push(objPersona);
    console.log(personas);
    listar();
    limpiar();
    //Obtener el tbody
    /*
    let lista = document.getElementById('lista');
    lista.innerHTML += `<tr>
    <td>`+ objPersona.nombre + `</td>
    <td>`+ objPersona.apellido + `</td>
    <td>`+ objPersona.email + `</td>
    <td>`+ objPersona.municipio + `</td>
    <td>
        <button type="button" class="btn btn-primary" onclick="editar()">Editar</button>
        <button type="button" class="btn btn-danger">Eliminar</button>
        </td>
    </tr>`
    */
}



function editar(fila) {
    console.log(fila);
    let elemento = document.getElementById(fila + 'c0')
    document.getElementById('nombre').value = elemento.innerText;
    elemento = document.getElementById(fila + 'c1')
    document.getElementById('apellido').value = elemento.innerText;
    elemento = document.getElementById(fila + 'c2')
    document.getElementById('email').value = elemento.innerText;
    elemento = document.getElementById(fila + 'c3')
    document.getElementById('municipios').value = elemento.innerText;
}


function btnActualizar() {
    console.log("actualizar");
    //Capturar datos del formulario
    let nombre = document.getElementById('nombre').value;
    let apellido = document.getElementById('apellido').value;
    let municipio = document.getElementById('municipios').value;
    let email = document.getElementById('email').value;
    //Buscar la persona por email
    personas.map(element => {
        if (element.email == email) {
            element.nombre = nombre;
            element.apellido = apellido;
            element.municipio = municipio;
        }
    });
    //Limpiar campos
    limpiar();
    //Recargar la tabla
    listar();
}

function btnEliminar(email){
    //Filtrar todas las personas que no tengan el email a eliminar
    let personasFiltro = personas.filter(element => (element.email != email));
    //Sobreescribir el arreglo global
    personas = personasFiltro;
    //Recargar la tabla
    listar();
}


/*
function registrar(){
    //Capturar los datos del formulario
    let nombre = document.getElementById('nombre').value;
    let apellido = document.getElementById('apellido').value;
    let email = document.getElementById('email').value;
    let municipio = document.getElementById('municipios').value;

    /***********************************************************************
    *Ejercicio:
    *Almacenar la info de la persona en el arreglo en forma de objeto
    *
    *************************************************************************/
/*
 let objPersona = {
    nombre,
    apellido,
    email,
    municipio
}
personas.push(objPersona);
console.log(personas);

//Ejercicio: Listar las personas en la tabla
}
*/

cargarMunicipios();